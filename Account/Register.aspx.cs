﻿using Microsoft.AspNet.Identity;
using System;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI;
using WiffleBall;

public partial class Account_Register : Page
{
    protected void CreateUser_Click(object sender, EventArgs e)
    {
        //MySql.Data.MySqlClient.MySqlConnection conn;
        //MySql.Data.MySqlClient.MySqlCommand cmd;
        SqlConnection conn;
        SqlCommand cmd;
        String queryStr;

        var manager = new UserManager();
        var user = new ApplicationUser() { UserName = UserName.Text };
        IdentityResult result = manager.Create(user, Password.Text);
        
        if (result.Succeeded)
        {
            //String connString = System.Configuration.ConfigurationManager.ConnectionStrings["wiffleAppConnString"].ToString();
            //conn = new MySql.Data.MySqlClient.MySqlConnection(connString);
            conn = new SqlConnection("workstation id=wiffleball.mssql.somee.com;packet size=4096;user id=cvaneron_SQLLogin_1;pwd=apl1pa8ije;data source=wiffleball.mssql.somee.com;persist security info=False;initial catalog=wiffleball");
            conn.Open();
            queryStr = "INSERT INTO users (user_name, singles, doubles, triples, hrs, rbis, k_batting, k_pitching, bb_batting, bb_pitching, runs) VALUES ('" + UserName.Text + "', 0, 0, 0, 0, 0, 0, 0, 0, 0,0)";
            cmd = new SqlCommand(queryStr, conn);
            cmd.ExecuteNonQuery();
            //cmd = new MySql.Data.MySqlClient.MySqlCommand(queryStr, conn);
            //cmd.ExecuteReader();
            conn.Close();
            IdentityHelper.SignIn(manager, user, isPersistent: false);
            IdentityHelper.RedirectToReturnUrl(Request.QueryString["ReturnUrl"], Response);
        }
        else
        {
            ErrorMessage.Text = result.Errors.FirstOrDefault();
        }
    }
}