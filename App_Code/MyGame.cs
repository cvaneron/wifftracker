﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Game class to keep track of indivual games as they are going on and used to store game results and update user stats in the db.
/// </summary>
public class MyGame
{
    int id;
    int outs;
    int inning;
    int home_team_singles;
    int home_team_doubles;
    int home_team_triples;
    int home_team_hrs;
    int home_team_rbis;
    int home_team_k_batting;
    int home_team_k_pitching;
    int home_team_bb_batting;
    int home_team_bb_pitching;
    String home_team_name;
    String away_team_name;
    String location;
    int away_team_singles;
    int away_team_doubles;
    int away_team_triples;
    int away_team_hrs;
    int away_team_rbis;
    int away_team_k_batting;
    int away_team_k_pitching;
    int away_team_bb_batting;
    int away_team_bb_pitching;

    int home_team_runs;
    int away_team_runs;

    String batting_team_name;
    String topOrBottom;

    bool[] runnersOn;

    public int Home_Team_Runs
    {
        get
        {
            return home_team_runs;
        }

        set
        {
            home_team_runs = value;
        }
    }

    public int Away_Team_Runs
    {
        get
        {
            return away_team_runs;
        }

        set
        {
            away_team_runs = value;
        }
    }

    public int Id
    {
        get
        {
            return id;
        }

        set
        {
            id = value;
        }
    }

    public int Outs
    {
        get
        {
            return outs;
        }

        set
        {
            outs = value;
        }
    }

    public int Inning
    {
        get
        {
            return inning;
        }

        set
        {
            inning = value;
        }
    }

    public int Home_team_singles
    {
        get
        {
            return home_team_singles;
        }

        set
        {
            home_team_singles = value;
        }
    }

    public int Home_team_doubles
    {
        get
        {
            return home_team_doubles;
        }

        set
        {
            home_team_doubles = value;
        }
    }

    public int Home_team_triples
    {
        get
        {
            return home_team_triples;
        }

        set
        {
            home_team_triples = value;
        }
    }

    public int Home_team_hrs
    {
        get
        {
            return home_team_hrs;
        }

        set
        {
            home_team_hrs = value;
        }
    }

    public int Home_team_rbis
    {
        get
        {
            return home_team_rbis;
        }

        set
        {
            home_team_rbis = value;
        }
    }

    public int Home_team_k_batting
    {
        get
        {
            return home_team_k_batting;
        }

        set
        {
            home_team_k_batting = value;
        }
    }

    public int Home_team_k_pitching
    {
        get
        {
            return home_team_k_pitching;
        }

        set
        {
            home_team_k_pitching = value;
        }
    }

    public int Home_team_bb_batting
    {
        get
        {
            return home_team_bb_batting;
        }

        set
        {
            home_team_bb_batting = value;
        }
    }

    public int Home_team_bb_pitching
    {
        get
        {
            return home_team_bb_pitching;
        }

        set
        {
            home_team_bb_pitching = value;
        }
    }

    public string TopOrBottom
    {
        get
        {
            return topOrBottom;
        }

        set
        {
            topOrBottom = value;
        }
    }

    public string Location
    {
        get
        {
            return location;
        }

        set
        {
            location = value;
        }
    }

    public string Batting_team_name
    {
        get
        {
            return batting_team_name;
        }

        set
        {
            batting_team_name = value;
        }
    }

    public string Home_team_name
    {
        get
        {
            return home_team_name;
        }

        set
        {
            home_team_name = value;
        }
    }

    public string Away_team_name
    {
        get
        {
            return away_team_name;
        }

        set
        {
            away_team_name = value;
        }
    }

    public int Away_team_singles
    {
        get
        {
            return away_team_singles;
        }

        set
        {
            away_team_singles = value;
        }
    }

    public int Away_team_doubles
    {
        get
        {
            return away_team_doubles;
        }

        set
        {
            away_team_doubles = value;
        }
    }

    public int Away_team_triples
    {
        get
        {
            return away_team_triples;
        }

        set
        {
            away_team_triples = value;
        }
    }

    public int Away_team_hrs
    {
        get
        {
            return away_team_hrs;
        }

        set
        {
            away_team_hrs = value;
        }
    }

    public int Away_team_rbis
    {
        get
        {
            return away_team_rbis;
        }

        set
        {
            away_team_rbis = value;
        }
    }

    public int Away_team_k_batting
    {
        get
        {
            return away_team_k_batting;
        }

        set
        {
            away_team_k_batting = value;
        }
    }

    public int Away_team_k_pitching
    {
        get
        {
            return away_team_k_pitching;
        }

        set
        {
            away_team_k_pitching = value;
        }
    }

    public int Away_team_bb_batting
    {
        get
        {
            return away_team_bb_batting;
        }

        set
        {
            away_team_bb_batting = value;
        }
    }

    public int Away_team_bb_pitching
    {
        get
        {
            return away_team_bb_pitching;
        }

        set
        {
            away_team_bb_pitching = value;
        }
    }

    public bool[] RunnersOn
    {
        get
        {
            return runnersOn;
        }

        set
        {
            runnersOn = value;
        }
    }

    public MyGame()
    {

    }

    public MyGame(String homeTeamName, String awayTeamName, String location)
    {
        id = 1;
        outs = 0;
        inning = 1;
        home_team_runs = 0;
        home_team_singles = 0;
        home_team_doubles = 0;
        home_team_triples = 0;
        home_team_hrs = 0;
        home_team_rbis = 0;
        home_team_k_batting = 0;
        home_team_k_pitching = 0;
        home_team_bb_batting = 0;
        home_team_bb_pitching = 0;
        home_team_name = homeTeamName;
        away_team_name = awayTeamName;
        this.location = location;
        away_team_runs = 0;
        away_team_singles = 0;
        away_team_doubles = 0;
        away_team_triples = 0;
        away_team_hrs = 0;
        away_team_rbis = 0;
        away_team_k_batting = 0;
        away_team_k_pitching = 0;
        away_team_bb_batting = 0;
        away_team_bb_pitching = 0;
        batting_team_name = away_team_name;
        topOrBottom = "Top";
        runnersOn = new bool[3];
    }

    public void switchBattingTeam ()
    {
        if (batting_team_name.Equals(away_team_name))
            batting_team_name = home_team_name;
        else
            batting_team_name = away_team_name;
    }
}