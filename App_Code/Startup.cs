﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WiffleBall.Startup))]
namespace WiffleBall
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
