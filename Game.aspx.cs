﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class Game : System.Web.UI.Page
{
    SqlConnection conn;
    SqlCommand cmd;
    SqlCommand cmd2;
    SqlCommand cmd3;


    // MySql.Data.MySqlClient.MySqlConnection conn;
   // MySql.Data.MySqlClient.MySqlCommand cmd;
   // MySql.Data.MySqlClient.MySqlCommand cmd2;
   // MySql.Data.MySqlClient.MySqlCommand cmd3;
    String queryStr;
    String queryStrHome;
    String queryStrAway;

    MyGame myGame = new MyGame();
    //MyGame myGame = (MyGame)Session["Game_Obj"];

    //bool[] runnersOn;

    protected void Page_Load(object sender, EventArgs e)
    {
        myGame = (MyGame)Session["Game_Obj"];
        nameOfHome.Text = " " + myGame.Home_team_name;
        nameOfAway.Text = " " + myGame.Away_team_name;
        // runnersOn = myGame.RunnersOn; .setValue for resetting
        batting_team_name.InnerHtml = myGame.Batting_team_name;
        top_or_bottom.InnerHtml = myGame.TopOrBottom;
        inning_num.InnerHtml = myGame.Inning.ToString();
    }

    protected void Home_Click(object sender, EventArgs e)
    {
        Response.Redirect("Default.aspx");
    }

    protected void Save_Results(object sender, EventArgs e)
    {
        //String connString = System.Configuration.ConfigurationManager.ConnectionStrings["wiffleAppConnString"].ToString();
        //conn = new MySql.Data.MySqlClient.MySqlConnection(connString);
        conn = new SqlConnection("workstation id=wiffleball.mssql.somee.com;packet size=4096;user id=cvaneron_SQLLogin_1;pwd=apl1pa8ije;data source=wiffleball.mssql.somee.com;persist security info=False;initial catalog=wiffleball");
        conn.Open();
        queryStr = "INSERT INTO game (home_team_name, away_team_name, location, home_team_singles, home_team_doubles, home_team_triples, home_team_hrs, home_team_rbis, home_team_k_batting, home_team_k_pitching, home_team_bb_batting, home_team_bb_pitching, away_team_singles, away_team_doubles, away_team_triples, away_team_hrs, away_team_rbis, away_team_k_batting, away_team_k_pitching, away_team_bb_batting, away_team_bb_pitching, home_team_runs, away_team_runs) VALUES ('" + myGame.Home_team_name + "', '" + myGame.Away_team_name + "', '" + myGame.Location + "', '" + myGame.Home_team_singles + "', '" + myGame.Home_team_doubles + "', '" + myGame.Home_team_triples + "', '" + myGame.Home_team_hrs + "', '" + myGame.Home_team_rbis + "', '" + myGame.Home_team_k_batting + "', '" + myGame.Home_team_k_pitching + "', '" + myGame.Home_team_bb_batting + "', '" + myGame.Home_team_bb_pitching + "', '" + myGame.Away_team_singles + "', '" + myGame.Away_team_doubles + "', '" + myGame.Away_team_triples + "', '" + myGame.Away_team_hrs + "', '" + myGame.Away_team_rbis + "', '" + myGame.Away_team_k_batting + "', '" + myGame.Away_team_k_pitching + "', '" + myGame.Away_team_bb_batting + "', '" + myGame.Away_team_bb_pitching + "', '" + myGame.Home_Team_Runs + "', '" + myGame.Away_Team_Runs + "')";
        queryStrHome = "UPDATE users SET singles = singles + " + myGame.Home_team_singles + ", doubles = doubles + " + myGame.Home_team_doubles + ", triples = triples + " + myGame.Home_team_triples + ", hrs = hrs + " + myGame.Home_team_hrs + ", rbis = rbis + " + myGame.Home_team_rbis + ", k_batting = k_batting + " + myGame.Home_team_k_batting + ", k_pitching = k_pitching + " + myGame.Home_team_k_pitching + ", bb_batting = bb_batting + " + myGame.Home_team_bb_batting + ", bb_pitching = bb_pitching + " + myGame.Home_team_bb_pitching + ", runs = runs + " + myGame.Home_Team_Runs + " WHERE user_name = '" + myGame.Home_team_name + "'";
        queryStrAway = "UPDATE users SET singles = singles + " + myGame.Away_team_singles + ", doubles = doubles + " + myGame.Away_team_doubles + ", triples = triples + " + myGame.Away_team_triples + ", hrs = hrs + " + myGame.Away_team_hrs + ", rbis = rbis + " + myGame.Away_team_rbis + ", k_batting = k_batting + " + myGame.Away_team_k_batting + ", k_pitching = k_pitching + " + myGame.Away_team_k_pitching + ", bb_batting = bb_batting + " + myGame.Away_team_bb_batting + ", bb_pitching = bb_pitching + " + myGame.Away_team_bb_pitching + ", runs = runs + " + myGame.Away_Team_Runs + " WHERE user_name = '" + myGame.Away_team_name + "'";
        //cmd = new MySql.Data.MySqlClient.MySqlCommand(queryStr, conn);
        cmd = new SqlCommand(queryStr, conn);
        cmd.ExecuteNonQuery();
        //cmd.ExecuteReader();
        conn.Close();
        conn.Open();
        cmd2 = new SqlCommand(queryStrHome, conn);
        cmd2.ExecuteNonQuery();
        //cmd2 = new MySql.Data.MySqlClient.MySqlCommand(queryStrHome, conn);
        //cmd2.ExecuteReader();
        conn.Close();
        conn.Open();
        cmd3 = new SqlCommand(queryStrAway, conn);
        cmd3.ExecuteNonQuery();
        //cmd3 = new MySql.Data.MySqlClient.MySqlCommand(queryStrAway, conn);
        //cmd3.ExecuteReader();
        conn.Close();
        save.Enabled = false;
    }

    protected void IncreaseSingles(object sender, EventArgs e)
    {
        if (myGame.Away_team_name.Equals(myGame.Batting_team_name)) //Away team is batting
        {
            myGame.Away_team_singles = myGame.Away_team_singles + 1;
        }
        else
            myGame.Home_team_singles = myGame.Home_team_singles + 1;

        updateRunnersOn("single");
      
        int totalAwayHits = myGame.Away_team_singles + myGame.Away_team_doubles + myGame.Away_team_triples + myGame.Away_team_hrs;
        awayHits.Text = totalAwayHits.ToString();
        int totalHomeHits = myGame.Home_team_singles + myGame.Home_team_doubles + myGame.Home_team_triples + myGame.Home_team_hrs;
        homeHits.Text = totalHomeHits.ToString();
    }

    protected void IncreaseDoubles(object sender, EventArgs e)
    {
        if (myGame.Away_team_name.Equals(myGame.Batting_team_name)) //Away team is batting
        {
            myGame.Away_team_doubles = myGame.Away_team_doubles + 1;
        }
        else
            myGame.Home_team_doubles = myGame.Home_team_doubles + 1;

        int totalAwayHits = myGame.Away_team_singles + myGame.Away_team_doubles + myGame.Away_team_triples + myGame.Away_team_hrs;
        awayHits.Text = totalAwayHits.ToString();
        int totalHomeHits = myGame.Home_team_singles + myGame.Home_team_doubles + myGame.Home_team_triples + myGame.Home_team_hrs;
        homeHits.Text = totalHomeHits.ToString();
        updateRunnersOn("double");
    }

    protected void IncreaseTriples(object sender, EventArgs e)
    {
        if (myGame.Away_team_name.Equals(myGame.Batting_team_name)) //Away team is batting
        {
            myGame.Away_team_triples = myGame.Away_team_triples + 1;
        }
        else
            myGame.Home_team_triples = myGame.Home_team_triples + 1;

        int totalAwayHits = myGame.Away_team_singles + myGame.Away_team_doubles + myGame.Away_team_triples + myGame.Away_team_hrs;
        awayHits.Text = totalAwayHits.ToString();
        int totalHomeHits = myGame.Home_team_singles + myGame.Home_team_doubles + myGame.Home_team_triples + myGame.Home_team_hrs;
        homeHits.Text = totalHomeHits.ToString();
        updateRunnersOn("triple");
    }

    protected void IncreaseHomeRuns(object sender, EventArgs e)
    {
        if (myGame.Away_team_name.Equals(myGame.Batting_team_name)) //Away team is batting
        {
            myGame.Away_team_hrs = myGame.Away_team_hrs + 1;
        }
        else
            myGame.Home_team_hrs = myGame.Home_team_hrs + 1;

        int totalAwayHits = myGame.Away_team_singles + myGame.Away_team_doubles + myGame.Away_team_triples + myGame.Away_team_hrs;
        awayHits.Text = totalAwayHits.ToString();
        int totalHomeHits = myGame.Home_team_singles + myGame.Home_team_doubles + myGame.Home_team_triples + myGame.Home_team_hrs;
        homeHits.Text = totalHomeHits.ToString();
        updateRunnersOn("hr");
    }

    protected void IncreaseStrikeOuts(object sender, EventArgs e)
    {
        if (myGame.Away_team_name.Equals(myGame.Batting_team_name)) //Away team is batting
        {
            myGame.Away_team_k_batting = myGame.Away_team_k_batting + 1;
            myGame.Home_team_k_pitching = myGame.Home_team_k_pitching + 1;
        }
        else
        {
            myGame.Home_team_k_batting = myGame.Home_team_k_batting + 1;
            myGame.Away_team_k_pitching = myGame.Away_team_k_pitching + 1;

        }
        myGame.Outs = myGame.Outs + 1;
        if (myGame.Outs == 2)
        {
            out1.InnerHtml = "";
            myGame.Outs = 0;

            if (myGame.TopOrBottom.Equals("Bottom"))
            {
                IncreaseInning();
                ChangeTopOrBottom();
            }
            else
                ChangeTopOrBottom();

            myGame.switchBattingTeam();
            updateRunnersOn("batterChange");
            //myGame.RunnersOn.SetValue(false, 0);
           // myGame.RunnersOn.SetValue(false, 1);
            //myGame.RunnersOn.SetValue(false, 2);
            

            batting_team_name.InnerHtml = myGame.Batting_team_name;
            top_or_bottom.InnerHtml = myGame.TopOrBottom;
            inning_num.InnerHtml = myGame.Inning.ToString();
        }
        else
            out1.InnerHtml = "X";
    }

    protected void IncreaseBB(object sender, EventArgs e)
    {
        if (myGame.Away_team_name.Equals(myGame.Batting_team_name)) //Away team is batting
        {
            myGame.Away_team_bb_batting = myGame.Away_team_bb_batting + 1;
            myGame.Home_team_bb_pitching = myGame.Home_team_bb_pitching + 1;
        }
        else
        {
            myGame.Home_team_bb_batting = myGame.Home_team_bb_batting + 1;
            myGame.Away_team_bb_pitching = myGame.Away_team_bb_pitching + 1;
        }

        updateRunnersOn("single");
    }

    protected void IncreaseOuts(object sender, EventArgs e)
    {
        myGame.Outs = myGame.Outs + 1;
        if (myGame.Outs == 2)
        {
            out1.InnerHtml = "";
            myGame.Outs = 0;

            if (myGame.TopOrBottom.Equals("Bottom"))
            {
                IncreaseInning();
                ChangeTopOrBottom();
            }
            else
                ChangeTopOrBottom();

            myGame.switchBattingTeam();
            updateRunnersOn("batterChange");
            //myGame.RunnersOn.SetValue(false, 0);
            //myGame.RunnersOn.SetValue(false, 1);
            //myGame.RunnersOn.SetValue(false, 2);
            
            

            batting_team_name.InnerHtml = myGame.Batting_team_name;
            top_or_bottom.InnerHtml = myGame.TopOrBottom;
            inning_num.InnerHtml = myGame.Inning.ToString();
        }
        else
            out1.InnerHtml = "X";
    }

    protected void ChangeTopOrBottom()
    {
        if (myGame.TopOrBottom.Equals("Top"))
        {
            int aRuns = myGame.Away_Team_Runs;
            int inn = myGame.Inning;
            updateScoreboard(inn, aRuns, "away");
            myGame.TopOrBottom = "Bottom";
        }
        else
        {
            int hRuns = myGame.Home_Team_Runs;
            int inn = myGame.Inning -1;
            updateScoreboard(inn, hRuns, "home");
            myGame.TopOrBottom = "Top";
        }
    }

    protected void updateScoreboard(int i, int r, String t)
    {
        int sub = 0;
        switch(i)
        {
            case 1:
                if (t.Equals("away"))
                    away1.Text = r.ToString();
                else
                    home1.Text = r.ToString();
                break;
            case 2:
                if (t.Equals("away"))
                {
                    sub = int.Parse(away1.Text);
                    sub = r - sub;
                    away2.Text = sub.ToString();
                }
                else
                {
                    sub = int.Parse(home1.Text);
                    sub = r - sub;
                    home2.Text = sub.ToString();
                }
                break;
            case 3:
                if (t.Equals("away"))
                {
                    sub = int.Parse(away1.Text);
                    int sub2 = int.Parse(away2.Text);
                    sub = sub + sub2;
                    sub = r - sub;
                    away3.Text = sub.ToString();
                }
                else
                {
                    sub = int.Parse(home1.Text);
                    int sub2 = int.Parse(home2.Text);
                    sub = sub + sub2;
                    sub = r - sub;
                    home3.Text = sub.ToString();
                }
                break;
            case 4:
                if (t.Equals("away"))
                {
                    sub = int.Parse(away1.Text);
                    int sub2 = int.Parse(away2.Text);
                    int sub3 = int.Parse(away3.Text);
                    sub = sub + sub2 + sub3;
                    sub = r - sub;
                    away4.Text = sub.ToString();
                }
                else
                {
                    sub = int.Parse(home1.Text);
                    int sub2 = int.Parse(home2.Text);
                    int sub3 = int.Parse(home3.Text);
                    sub = sub + sub2 + sub3;
                    sub = r - sub;
                    home4.Text = sub.ToString();
                }
                break;
            case 5:
                if (t.Equals("away"))
                {
                    sub = int.Parse(away1.Text);
                    int sub2 = int.Parse(away2.Text);
                    int sub3 = int.Parse(away3.Text);
                    int sub4 = int.Parse(away4.Text);
                    sub = sub + sub2 + sub3 + sub4;
                    sub = r - sub;
                    away5.Text = sub.ToString();
                }
                else
                {
                    sub = int.Parse(home1.Text);
                    int sub2 = int.Parse(home2.Text);
                    int sub3 = int.Parse(home3.Text);
                    int sub4 = int.Parse(home4.Text);
                    sub = sub + sub2 + sub3 + sub4;
                    sub = r - sub;
                    home5.Text = sub.ToString();
                }
                break;
            case 6:
                if (t.Equals("away"))
                {
                    sub = int.Parse(away1.Text);
                    int sub2 = int.Parse(away2.Text);
                    int sub3 = int.Parse(away3.Text);
                    int sub4 = int.Parse(away4.Text);
                    int sub5 = int.Parse(away5.Text);
                    sub = sub + sub2 + sub3 + sub4 + sub5;
                    sub = r - sub;
                    away6.Text = sub.ToString();
                }
                else
                {
                    sub = int.Parse(home1.Text);
                    int sub2 = int.Parse(home2.Text);
                    int sub3 = int.Parse(home3.Text);
                    int sub4 = int.Parse(home4.Text);
                    int sub5 = int.Parse(home5.Text);
                    sub = sub + sub2 + sub3 + sub4 + sub5;
                    sub = r - sub;
                    home6.Text = sub.ToString();
                }
                break;
            case 7:
                if (t.Equals("away"))
                {
                    sub = int.Parse(away1.Text);
                    int sub2 = int.Parse(away2.Text);
                    int sub3 = int.Parse(away3.Text);
                    int sub4 = int.Parse(away4.Text);
                    int sub5 = int.Parse(away5.Text);
                    int sub6 = int.Parse(away6.Text);
                    sub = sub + sub2 + sub3 + sub4 + sub5 + sub6;
                    sub = r - sub;
                    away7.Text = sub.ToString();
                }
                else
                {
                    sub = int.Parse(home1.Text);
                    int sub2 = int.Parse(home2.Text);
                    int sub3 = int.Parse(home3.Text);
                    int sub4 = int.Parse(home4.Text);
                    int sub5 = int.Parse(home5.Text);
                    int sub6 = int.Parse(home6.Text);
                    sub = sub + sub2 + sub3 + sub4 + sub5 + sub6;
                    sub = r - sub;
                    home7.Text = sub.ToString();
                }
                break;
            case 8:
                if (t.Equals("away"))
                {
                    sub = int.Parse(away1.Text);
                    int sub2 = int.Parse(away2.Text);
                    int sub3 = int.Parse(away3.Text);
                    int sub4 = int.Parse(away4.Text);
                    int sub5 = int.Parse(away5.Text);
                    int sub6 = int.Parse(away6.Text);
                    int sub7 = int.Parse(away7.Text);
                    sub = sub + sub2 + sub3 + sub4 + sub5 + sub6 + sub7;
                    sub = r - sub;
                    away8.Text = sub.ToString();
                }
                else
                {
                    sub = int.Parse(home1.Text);
                    int sub2 = int.Parse(home2.Text);
                    int sub3 = int.Parse(home3.Text);
                    int sub4 = int.Parse(home4.Text);
                    int sub5 = int.Parse(home5.Text);
                    int sub6 = int.Parse(home6.Text);
                    int sub7 = int.Parse(home7.Text);
                    sub = sub + sub2 + sub3 + sub4 + sub5 + sub6 + sub7;
                    sub = r - sub;
                    home8.Text = sub.ToString();
                }
                break;
            case 9:
                if (t.Equals("away"))
                {
                    sub = int.Parse(away1.Text);
                    int sub2 = int.Parse(away2.Text);
                    int sub3 = int.Parse(away3.Text);
                    int sub4 = int.Parse(away4.Text);
                    int sub5 = int.Parse(away5.Text);
                    int sub6 = int.Parse(away6.Text);
                    int sub7 = int.Parse(away7.Text);
                    int sub8 = int.Parse(away8.Text);
                    sub = sub + sub2 + sub3 + sub4 + sub5 + sub6 + sub7 + sub8;
                    sub = r - sub;
                    away9.Text = sub.ToString();
                }
                else
                {
                    sub = int.Parse(home1.Text);
                    int sub2 = int.Parse(home2.Text);
                    int sub3 = int.Parse(home3.Text);
                    int sub4 = int.Parse(home4.Text);
                    int sub5 = int.Parse(home5.Text);
                    int sub6 = int.Parse(home6.Text);
                    int sub7 = int.Parse(home7.Text);
                    int sub8 = int.Parse(home8.Text);
                    sub = sub + sub2 + sub3 + sub4 + sub5 + sub6 + sub7 + sub8;
                    sub = r - sub;
                    home9.Text = sub.ToString();
                }
                break;
        }

        if (t.Equals("away"))
            awayRuns.Text = r.ToString();
        else
            homeRuns.Text = r.ToString();

        if (t.Equals("away"))
        {
            int totalAwayHits = myGame.Away_team_singles + myGame.Away_team_doubles + myGame.Away_team_triples + myGame.Away_team_hrs;
            awayHits.Text = totalAwayHits.ToString();
        } 
        else
        {
            int totalHomeHits = myGame.Home_team_singles + myGame.Home_team_doubles + myGame.Home_team_triples + myGame.Home_team_hrs;
            homeHits.Text = totalHomeHits.ToString();
        }
    }

    protected void IncreaseInning()
    {
        myGame.Inning = myGame.Inning + 1;
        if (myGame.Inning > 7 && myGame.Home_Team_Runs != myGame.Away_Team_Runs)
        {
            myGame.Inning = myGame.Inning - 1;
            popupPanel.Visible = true;
            outButton.Visible = false;
            playerImg.Visible = false;
        }
    }

    protected void updateRuns(int num)
    {
        if(myGame.Batting_team_name.Equals(myGame.Home_team_name))
        {
            myGame.Home_Team_Runs = myGame.Home_Team_Runs + num;
        }
        else
        {
            myGame.Away_Team_Runs = myGame.Away_Team_Runs + num;
        }

        homeRuns.Text = myGame.Home_Team_Runs.ToString();
        awayRuns.Text = myGame.Away_Team_Runs.ToString();
    }

    protected void updateRunnersOn(String str)
    {
        switch (str)
        {
            case "single":
                if (myGame.RunnersOn.GetValue(0).Equals(false) && myGame.RunnersOn.GetValue(1).Equals(false) && myGame.RunnersOn.GetValue(2).Equals(false)) // nobody on base
                {
                    myGame.RunnersOn.SetValue(true, 0);
                    runner1.Visible = true;
                }
                else if (myGame.RunnersOn.GetValue(0).Equals(true) && myGame.RunnersOn.GetValue(1).Equals(false) && myGame.RunnersOn.GetValue(2).Equals(false)) // man on first
                {
                    myGame.RunnersOn.SetValue(true, 1);
                    runner2.Visible = true;
                }
                else if (myGame.RunnersOn.GetValue(0).Equals(true) && myGame.RunnersOn.GetValue(1).Equals(true) && myGame.RunnersOn.GetValue(2).Equals(false)) // man on first and second
                {
                    myGame.RunnersOn.SetValue(true, 2);
                    runner3.Visible = true;
                }
                else if (myGame.RunnersOn.GetValue(0).Equals(true) && myGame.RunnersOn.GetValue(1).Equals(false) && myGame.RunnersOn.GetValue(2).Equals(true)) // man on first and third
                {
                    myGame.RunnersOn.SetValue(true, 1);
                    runner2.Visible = true;
                    myGame.RunnersOn.SetValue(false, 2);
                    runner3.Visible = false;
                    updateRuns(1);
                }
                else if (myGame.RunnersOn.GetValue(0).Equals(false) && myGame.RunnersOn.GetValue(1).Equals(true) && myGame.RunnersOn.GetValue(2).Equals(false)) // man on second
                {
                    myGame.RunnersOn.SetValue(true, 0);
                    runner1.Visible = true;
                    myGame.RunnersOn.SetValue(false, 1);
                    runner2.Visible = false;
                    myGame.RunnersOn.SetValue(true, 2);
                    runner3.Visible = true;
                }
                else if (myGame.RunnersOn.GetValue(0).Equals(false) && myGame.RunnersOn.GetValue(1).Equals(true) && myGame.RunnersOn.GetValue(2).Equals(true)) // man on second and third
                {
                    myGame.RunnersOn.SetValue(true, 0);
                    runner1.Visible = true;
                    myGame.RunnersOn.SetValue(false, 1);
                    runner2.Visible = false;
                    updateRuns(1);
                }
                else if (myGame.RunnersOn.GetValue(0).Equals(false) && myGame.RunnersOn.GetValue(1).Equals(false) && myGame.RunnersOn.GetValue(2).Equals(true)) // man on third
                {
                    myGame.RunnersOn.SetValue(true, 0);
                    runner1.Visible = true;
                    myGame.RunnersOn.SetValue(false, 2);
                    runner3.Visible = false;
                    updateRuns(1);
                }
                else if (myGame.RunnersOn.GetValue(0).Equals(true) && myGame.RunnersOn.GetValue(1).Equals(true) && myGame.RunnersOn.GetValue(2).Equals(true)) // bases loaded
                {
                    updateRuns(1);
                }
                break;
            case "double":
                if (myGame.RunnersOn.GetValue(0).Equals(false) && myGame.RunnersOn.GetValue(1).Equals(false) && myGame.RunnersOn.GetValue(2).Equals(false)) // nobody on base
                {
                    myGame.RunnersOn.SetValue(true, 1);
                    runner2.Visible = true;
                }
                else if (myGame.RunnersOn.GetValue(0).Equals(true) && myGame.RunnersOn.GetValue(1).Equals(false) && myGame.RunnersOn.GetValue(2).Equals(false)) // man on first
                {
                    myGame.RunnersOn.SetValue(false, 0);
                    runner1.Visible = false;
                    myGame.RunnersOn.SetValue(true, 1);
                    runner2.Visible = true;
                    myGame.RunnersOn.SetValue(true, 2);
                    runner3.Visible = true;
                }
                else if (myGame.RunnersOn.GetValue(0).Equals(true) && myGame.RunnersOn.GetValue(1).Equals(true) && myGame.RunnersOn.GetValue(2).Equals(false)) // man on first and second
                {
                    myGame.RunnersOn.SetValue(false, 0);
                    runner1.Visible = false;
                    myGame.RunnersOn.SetValue(true, 1);
                    runner2.Visible = true;
                    myGame.RunnersOn.SetValue(true, 2);
                    runner3.Visible = true;
                    updateRuns(1);
                }
                else if (myGame.RunnersOn.GetValue(0).Equals(true) && myGame.RunnersOn.GetValue(1).Equals(false) && myGame.RunnersOn.GetValue(2).Equals(true)) // man on first and third
                {
                    myGame.RunnersOn.SetValue(false, 0);
                    runner1.Visible = false;
                    myGame.RunnersOn.SetValue(true, 1);
                    runner2.Visible = true;
                    updateRuns(1);
                }
                else if (myGame.RunnersOn.GetValue(0).Equals(false) && myGame.RunnersOn.GetValue(1).Equals(true) && myGame.RunnersOn.GetValue(2).Equals(false)) // man on second
                {
                    updateRuns(1);
                }
                else if (myGame.RunnersOn.GetValue(0).Equals(false) && myGame.RunnersOn.GetValue(1).Equals(true) && myGame.RunnersOn.GetValue(2).Equals(true)) // man on second and third
                {
                    myGame.RunnersOn.SetValue(false, 2);
                    runner3.Visible = false;
                    updateRuns(2);
                }
                else if (myGame.RunnersOn.GetValue(0).Equals(false) && myGame.RunnersOn.GetValue(1).Equals(false) && myGame.RunnersOn.GetValue(2).Equals(true)) // man on third
                {
                    myGame.RunnersOn.SetValue(true, 1);
                    runner2.Visible = true;
                    myGame.RunnersOn.SetValue(false, 2);
                    runner3.Visible = false;
                    updateRuns(1);
                }
                else if (myGame.RunnersOn.GetValue(0).Equals(true) && myGame.RunnersOn.GetValue(1).Equals(true) && myGame.RunnersOn.GetValue(2).Equals(true)) // bases loaded
                {
                    myGame.RunnersOn.SetValue(false, 0);
                    runner1.Visible = false;
                    updateRuns(2);
                }
                break;
            case "triple":
                if (myGame.RunnersOn.GetValue(0).Equals(false) && myGame.RunnersOn.GetValue(1).Equals(false) && myGame.RunnersOn.GetValue(2).Equals(false)) // nobody on base
                {
                    myGame.RunnersOn.SetValue(true, 2);
                    runner3.Visible = true;
                }
                else if (myGame.RunnersOn.GetValue(0).Equals(true) && myGame.RunnersOn.GetValue(1).Equals(false) && myGame.RunnersOn.GetValue(2).Equals(false)) // man on first
                {
                    myGame.RunnersOn.SetValue(false, 0);
                    runner1.Visible = false;
                    myGame.RunnersOn.SetValue(true, 2);
                    runner3.Visible = true;
                    updateRuns(1);
                }
                else if (myGame.RunnersOn.GetValue(0).Equals(true) && myGame.RunnersOn.GetValue(1).Equals(true) && myGame.RunnersOn.GetValue(2).Equals(false)) // man on first and second
                {
                    myGame.RunnersOn.SetValue(false, 0);
                    runner1.Visible = false;
                    myGame.RunnersOn.SetValue(false, 1);
                    runner2.Visible = false;
                    myGame.RunnersOn.SetValue(true, 2);
                    runner3.Visible = true;
                    updateRuns(2);
                }
                else if (myGame.RunnersOn.GetValue(0).Equals(true) && myGame.RunnersOn.GetValue(1).Equals(false) && myGame.RunnersOn.GetValue(2).Equals(true)) // man on first and third
                {
                    myGame.RunnersOn.SetValue(false, 0);
                    runner1.Visible = false;
                    updateRuns(2);
                }
                else if (myGame.RunnersOn.GetValue(0).Equals(false) && myGame.RunnersOn.GetValue(1).Equals(true) && myGame.RunnersOn.GetValue(2).Equals(false)) // man on second
                {
                    myGame.RunnersOn.SetValue(false, 1);
                    runner2.Visible = false;
                    myGame.RunnersOn.SetValue(true, 2);
                    runner3.Visible = true;
                    updateRuns(1);
                }
                else if (myGame.RunnersOn.GetValue(0).Equals(false) && myGame.RunnersOn.GetValue(1).Equals(true) && myGame.RunnersOn.GetValue(2).Equals(true)) // man on second and third
                {
                    myGame.RunnersOn.SetValue(false, 1);
                    runner2.Visible = false;
                    updateRuns(2);
                }
                else if (myGame.RunnersOn.GetValue(0).Equals(false) && myGame.RunnersOn.GetValue(1).Equals(false) && myGame.RunnersOn.GetValue(2).Equals(true)) // man on third
                {
                    updateRuns(1);
                }
                else if (myGame.RunnersOn.GetValue(0).Equals(true) && myGame.RunnersOn.GetValue(1).Equals(true) && myGame.RunnersOn.GetValue(2).Equals(true)) // bases loaded
                {
                    myGame.RunnersOn.SetValue(false, 0);
                    runner1.Visible = false;
                    myGame.RunnersOn.SetValue(false, 1);
                    runner2.Visible = false;
                    updateRuns(3);
                }
                break;
            case "hr":
                if (myGame.RunnersOn.GetValue(0).Equals(false) && myGame.RunnersOn.GetValue(1).Equals(false) && myGame.RunnersOn.GetValue(2).Equals(false)) // nobody on base
                {
                    updateRuns(1);
                }
                else if (myGame.RunnersOn.GetValue(0).Equals(true) && myGame.RunnersOn.GetValue(1).Equals(false) && myGame.RunnersOn.GetValue(2).Equals(false)) // man on first
                {
                    myGame.RunnersOn.SetValue(false, 0);
                    runner1.Visible = false;
                    updateRuns(2);
                }
                else if (myGame.RunnersOn.GetValue(0).Equals(true) && myGame.RunnersOn.GetValue(1).Equals(true) && myGame.RunnersOn.GetValue(2).Equals(false)) // man on first and second
                {
                    myGame.RunnersOn.SetValue(false, 0);
                    runner1.Visible = false;
                    myGame.RunnersOn.SetValue(false, 1);
                    runner2.Visible = false;
                    updateRuns(3);
                }
                else if (myGame.RunnersOn.GetValue(0).Equals(true) && myGame.RunnersOn.GetValue(1).Equals(false) && myGame.RunnersOn.GetValue(2).Equals(true)) // man on first and third
                {
                    myGame.RunnersOn.SetValue(false, 0);
                    runner1.Visible = false;
                    myGame.RunnersOn.SetValue(false, 2);
                    runner3.Visible = false;
                    updateRuns(3);
                }
                else if (myGame.RunnersOn.GetValue(0).Equals(false) && myGame.RunnersOn.GetValue(1).Equals(true) && myGame.RunnersOn.GetValue(2).Equals(false)) // man on second
                {
                    myGame.RunnersOn.SetValue(false, 0);
                    runner1.Visible = false;
                    updateRuns(2);
                }
                else if (myGame.RunnersOn.GetValue(0).Equals(false) && myGame.RunnersOn.GetValue(1).Equals(true) && myGame.RunnersOn.GetValue(2).Equals(true)) // man on second and third
                {
                    myGame.RunnersOn.SetValue(false, 1);
                    runner2.Visible = false;
                    myGame.RunnersOn.SetValue(false, 2);
                    runner3.Visible = false;
                    updateRuns(3);
                }
                else if (myGame.RunnersOn.GetValue(0).Equals(false) && myGame.RunnersOn.GetValue(1).Equals(false) && myGame.RunnersOn.GetValue(2).Equals(true)) // man on third
                {
                    myGame.RunnersOn.SetValue(false, 2);
                    runner3.Visible = false;
                    updateRuns(2);
                }
                else if (myGame.RunnersOn.GetValue(0).Equals(true) && myGame.RunnersOn.GetValue(1).Equals(true) && myGame.RunnersOn.GetValue(2).Equals(true)) // bases loaded
                {
                    myGame.RunnersOn.SetValue(false, 0);
                    runner1.Visible = false;
                    myGame.RunnersOn.SetValue(false, 1);
                    runner2.Visible = false;
                    myGame.RunnersOn.SetValue(false, 2);
                    runner3.Visible = false;
                    updateRuns(4);
                }
                break;
            case "batterChange":
                myGame.RunnersOn.SetValue(false, 0);
                myGame.RunnersOn.SetValue(false, 1);
                myGame.RunnersOn.SetValue(false, 2);
                runner1.Visible = false;
                runner2.Visible = false;
                runner3.Visible = false;
                break;
        }
    }
}