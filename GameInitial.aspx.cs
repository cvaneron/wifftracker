﻿//using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class GameInitial : System.Web.UI.Page
{
    //MySql.Data.MySqlClient.MySqlConnection conn;
    //MySql.Data.MySqlClient.MySqlCommand cmd;
    String queryStr;
    SqlConnection conn;
    SqlCommand cmd;
  
    protected void Page_Load(object sender, EventArgs e)
    {
        conn = new SqlConnection("workstation id=wiffleball.mssql.somee.com;packet size=4096;user id=cvaneron_SQLLogin_1;pwd=apl1pa8ije;data source=wiffleball.mssql.somee.com;persist security info=False;initial catalog=wiffleball");

        try
        {
            conn.Open();
        }
        catch(Exception)
        {
            Console.WriteLine("ERROR");
        }

        SqlDataReader myReader = null;
        cmd = new SqlCommand("select user_name from users", conn);
        myReader = cmd.ExecuteReader();
        while (myReader.Read())
        {
            HomeTeamList.Items.Add(myReader.GetString(0));
            AwayTeamList.Items.Add(myReader.GetString(0));
        }
        conn.Close();
    }

    protected void StartGame_Click(object sender, EventArgs e)
    {
        MyGame thisGame = new MyGame(HomeTeamList.SelectedItem.Text, AwayTeamList.SelectedItem.Text, LocationTextBox.Text); // creating game object
        thisGame.Batting_team_name = AwayTeamList.SelectedItem.Text;
        Session["Game_Obj"] = thisGame;
        Span1.InnerHtml = HomeTeamList.SelectedItem.Text;
        // Redirect to second page.
        Response.Redirect("Game.aspx");
        // Server.Transfer("Game.aspx");
    }
}
