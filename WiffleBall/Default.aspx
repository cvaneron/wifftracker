﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeadContent" runat="server">
    <link rel="stylesheet" type="text/css" href="/Styles/StyleSheet2.css" />
</asp:Content>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div id="default_wrapper">
        <h4>Welcome to WiffTracker! The application that let's you track your Wiffleball statistics during and after the game.</h4>
        <br />
        <p>First, be sure to log in or register if you haven&#39;t done so.</p>
        <br />
        <p>To begin a game, click the Game tab from the navigation bar.</p>
        <p>You will be prompted to select the Home Team, the Away Team, and the Location.</p>
        <p>Clicking Start Game will direct you to the Game Screen where you can select the appropriate button disguised as a baseball depeneding on the outcome of the current at bat.</p>
        <br />
        <p>Be sure to save the results when prompted at the end for the game to be saved and your individual stats to be updated.</p>
        <p>ENJOY!!!</p>
    </div>
</asp:Content>
