﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Game.aspx.cs" Inherits="Game" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <link rel="stylesheet" type="text/css" href="/Styles/StyleSheet3.css" />
    <title>Game Page</title>
</head>
<body>
    <form id="form1" runat="server">
        <div id="content">
            <div id="topBar">
                <div id="left">
                    <h3><span id="batting_team_name" runat="server" /> is batting.</h3>
                </div>
                <div id="right">
                    <h1><asp:HyperLink ID="quitLink" runat="server" NavigateUrl="~/Default.aspx">Quit</asp:HyperLink> <span id="top_or_bottom" runat="server" /> <span id="inning_num" runat="server" /></h1>
                </div>
            </div>
            <div id="button_section">
                <!--  <asp:Image ID="gameBackgroundImg" runat="server" ImageUrl="/Images/baseballDiamond.jpg" BorderColor="ForestGreen" /> -->
                <div id="top_btns">
                    <div id="singleBtn">
                        <asp:Button ID="single" class="gameBtn" runat="server" Text="1B" onclick="IncreaseSingles"/>
                    </div>
                    <div id="doubleBtn">
                        <asp:Button ID="double" class="gameBtn" runat="server" Text="2B" onclick="IncreaseDoubles"/>
                    </div>
                    <div id="tripleBtn">
                        <asp:Button ID="triple" class="gameBtn" runat="server" Text="3B" onclick="IncreaseTriples"/>
                    </div>
                    <div id="hrBtn">
                        <asp:Button ID="hr" class="gameBtn" runat="server" Text="HR" onclick="IncreaseHomeRuns" />
                    </div>
                </div>

                <div id="middle_btns">
                    <div id="strikeoutBtn">
                        <asp:Button ID="strikeout" class="gameBtn" runat="server" Text="K"  onclick="IncreaseStrikeOuts" />
                    </div>
                    <div id="walkBtn">
                        <asp:Button ID="walk" class="gameBtn" runat="server" Text="BB"  onclick="IncreaseBB" />
                    </div>
                </div>
                

                <!--  <asp:Button ID="goHome" runat="server" Text="Return Home" OnClick="Home_Click" /> -->

                <div id="outs_section">
                   <h1>Outs</h1>
                   <div id="circle"><div id="text"><span id="out1" runat="server" /></div></div>
                   <div id="circle2"></div>
                    <div id="outBtn">
                        <asp:Button ID="outButton" class="gameBtn" runat="server" Text="Out" onclick="IncreaseOuts" />
                    </div>
                </div>
            

          
              <asp:Panel ID="popupPanel" CssClass="popup" runat="server" Visible="false">
                  <h1>Game Completed</h1>
                <div id="saveBtn">
                    <asp:Button ID="save" runat="server" Text="Save Results" onClick="Save_Results" BackColor="White" BorderColor="Red" Width="120" Font-Size="Large"/>
                </div>
                <div id="quitBtn">
                    <asp:Button ID="quit" runat="server" Text="Quit" OnClick="Home_Click" BackColor="White" BorderColor="Red" Width="120" Font-Size="Large"/>
                </div>
              </asp:Panel>
                
                <asp:Table ID="scoreboard" runat="server" BorderColor="Black" BorderWidth="10">
                    <asp:TableRow BorderColor="Black" BorderWidth="5" ForeColor="Navy" Font-Bold="true" Height="20">
                        <asp:TableCell></asp:TableCell>
                        <asp:TableCell>1</asp:TableCell>
                        <asp:TableCell>2</asp:TableCell>
                        <asp:TableCell>3</asp:TableCell>
                        <asp:TableCell>4</asp:TableCell>
                        <asp:TableCell>5</asp:TableCell>
                        <asp:TableCell>6</asp:TableCell>
                        <asp:TableCell>7</asp:TableCell>
                        <asp:TableCell>8</asp:TableCell>
                        <asp:TableCell>9</asp:TableCell>
                        <asp:TableCell>R</asp:TableCell>
                        <asp:TableCell>H</asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow BorderColor="Black" BorderWidth="5">
                        <asp:TableCell ID="nameOfHome" runat="server" BackColor="Black" ForeColor="White">Home Team</asp:TableCell>
                        <asp:TableCell ID="home1" runat="server"></asp:TableCell>
                        <asp:TableCell ID="home2" runat="server"></asp:TableCell>
                        <asp:TableCell ID="home3" runat="server"></asp:TableCell>
                        <asp:TableCell ID="home4" runat="server"></asp:TableCell>
                        <asp:TableCell ID="home5" runat="server"></asp:TableCell>
                        <asp:TableCell ID="home6" runat="server"></asp:TableCell>
                        <asp:TableCell ID="home7" runat="server"></asp:TableCell>
                        <asp:TableCell ID="home8" runat="server"></asp:TableCell>
                        <asp:TableCell ID="home9" runat="server"></asp:TableCell>
                        <asp:TableCell ID="homeRuns" runat="server">0</asp:TableCell>
                        <asp:TableCell ID="homeHits" runat="server">0</asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow BorderColor="Black" BorderWidth="5">
                        <asp:TableCell ID="nameOfAway" runat="server" BackColor="Black" ForeColor="White">Away Team</asp:TableCell>
                        <asp:TableCell ID="away1" runat="server"></asp:TableCell>
                        <asp:TableCell ID="away2" runat="server"></asp:TableCell>
                        <asp:TableCell ID="away3" runat="server"></asp:TableCell>
                        <asp:TableCell ID="away4" runat="server"></asp:TableCell>
                        <asp:TableCell ID="away5" runat="server"></asp:TableCell>
                        <asp:TableCell ID="away6" runat="server"></asp:TableCell>
                        <asp:TableCell ID="away7" runat="server"></asp:TableCell>
                        <asp:TableCell ID="away8" runat="server"></asp:TableCell>
                        <asp:TableCell ID="away9" runat="server"></asp:TableCell>
                        <asp:TableCell ID="awayRuns" runat="server">0</asp:TableCell>
                        <asp:TableCell ID="awayHits" runat="server">0</asp:TableCell>
                    </asp:TableRow>
                </asp:Table>  
                
                <asp:Image ID="playerImg" runat="server" ImageUrl="Images/baseballPlayer1.png" />    
                <asp:Image ID="runner1" runat="server" ImageUrl="Images/baseRunnerLeft.png" Visible="false" />
                <asp:Image ID="runner2" runat="server" ImageUrl="Images/baseRunnerLeft.png" Visible="false" />
                <asp:Image ID="runner3" runat="server" ImageUrl="Images/baseRunnerRight.png" Visible="false" />     
            </div>
        </div>
    </form>
</body>
</html>
