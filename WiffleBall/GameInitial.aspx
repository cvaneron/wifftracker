﻿<%@ Page Title="Game Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="GameInitial.aspx.cs" Inherits="GameInitial" %>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeadContent" runat="server">
    <link rel="stylesheet" type="text/css" href="/Styles/StyleSheet2.css" />
</asp:Content>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div id ="gameWrapper">
        <div id ="TeamSelectArea">
            <div id="HomeTeamSelect">
                <label for="HomeTeamList">Select Home Team</label>
              <asp:DropDownList ID="HomeTeamList" runat="server"  width="130" font-size="12">
                </asp:DropDownList>
            </div>
            <div id="AwayTeamSelect">
                <label for="AwayTeamList">Select Away Team</label>
                <asp:DropDownList ID="AwayTeamList" runat="server" width="130" font-size="12">
                </asp:DropDownList>
            </div>
            
        </div>

        <div id ="LocationArea">
            <label for ="LocationText">Enter Location</label>
            <asp:TextBox ID="LocationTextBox" runat="server"></asp:TextBox>

            <span id="changed_location" runat="server" />

            <span id="Span1" runat="server" />
        </div>

        <div id ="StartGameArea">
            <asp:Button ID="StartGameBtn" runat="server" Text="Start Game" onclick="StartGame_Click" BackColor="White" BorderColor="Red" Width="240" Font-Size="Large"/>
        </div>
    </div>    
</asp:Content>
