﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class GameInitial : System.Web.UI.Page
{
    MySql.Data.MySqlClient.MySqlConnection conn;
    MySql.Data.MySqlClient.MySqlCommand cmd;
    String queryStr;
  
    protected void Page_Load(object sender, EventArgs e)
    {
        String connString = System.Configuration.ConfigurationManager.ConnectionStrings["wiffleAppConnString"].ToString();
        conn = new MySql.Data.MySqlClient.MySqlConnection(connString);
        conn.Open();
        queryStr = "SELECT user_name FROM wiffleball.users";
        cmd = new MySql.Data.MySqlClient.MySqlCommand(queryStr, conn);
        try
        {
            MySqlDataReader reader = cmd.ExecuteReader();
            while(reader.Read())
            {
                HomeTeamList.Items.Add(reader.GetString(0));
                AwayTeamList.Items.Add(reader.GetString(0));
            }
        }
        catch(MySqlException)
        {

        }
        conn.Close();
    }

    protected void StartGame_Click(object sender, EventArgs e)
    {
        MyGame thisGame = new MyGame(HomeTeamList.SelectedItem.Text, AwayTeamList.SelectedItem.Text, LocationTextBox.Text); // creating game object
        thisGame.Batting_team_name = AwayTeamList.SelectedItem.Text;
        Session["Game_Obj"] = thisGame;
        Span1.InnerHtml = HomeTeamList.SelectedItem.Text;
        // Redirect to second page.
        Response.Redirect("Game.aspx");
        // Server.Transfer("Game.aspx");
    }
}
